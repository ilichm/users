package com.bci.usuarios.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bci.usuarios.exception.PasswordFormatException;
import com.bci.usuarios.model.request.CreateUserRequest;
import com.bci.usuarios.model.request.UpdateUserRequest;
import com.bci.usuarios.model.response.CreateUserResponse;
import com.bci.usuarios.model.response.DeleteUserResponse;
import com.bci.usuarios.model.response.GetUserResponse;
import com.bci.usuarios.model.response.UpdateUserResponse;
import com.bci.usuarios.service.UserService;

@RestController
@RequestMapping(value="/api/v1")
public class UserController {
	
	@Autowired
	private UserService service;
	
	@Value("${format.password}")
	private String formatPass;
	
	@PostMapping(value="/users", produces="application/json", consumes="application/json")
	public ResponseEntity<CreateUserResponse> createUser(@RequestBody @Valid CreateUserRequest req) {
		
		validPass(req.getPassword());
		CreateUserResponse resp = service.createUser(req);
		return new ResponseEntity<>(resp,HttpStatus.CREATED);
	}
	
	@GetMapping(value="/users/{id}", produces="application/json")
	@ResponseBody
	public ResponseEntity<Object> getUser(@PathVariable int id) {
		GetUserResponse user = service.getUserById(id);
		return new ResponseEntity<>(user,HttpStatus.OK);
	}
	
	@PutMapping(value="/users/{id}", consumes="application/json")
	public ResponseEntity<Object> updateUser(@RequestBody @Valid UpdateUserRequest req, @PathVariable Integer id) {
		validPass(req.getPassword());
		service.updateUser(req, id);
		return new ResponseEntity<>(new UpdateUserResponse("Usuario actualizado existosamente"),HttpStatus.OK);
	}
	
	@DeleteMapping(value="/users/{id}", produces="application/json")
	@ResponseBody
	public ResponseEntity<Object> deleteUser(@PathVariable Integer id) {
		service.deleteUser(id);
		return new ResponseEntity<>(new DeleteUserResponse("Usuario eliminado exitosamente"),HttpStatus.OK);
	}
	
	private void validPass(String pass) {
		Pattern pat = Pattern.compile(formatPass);
		Matcher mat = pat.matcher(pass); 
		if (!mat.matches()) {
			throw new PasswordFormatException("Error en el formato del password");
		} 
	}
}
