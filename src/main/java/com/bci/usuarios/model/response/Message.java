package com.bci.usuarios.model.response;

public class Message {

private String mensaje;
	
	public Message(String desc) {
		this.mensaje = desc;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
