package com.bci.usuarios.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetUserResponse {
	
	private Integer id;
	private String name;
	private String created;
	private String modified;
	@JsonProperty("last_login")
	private String lastLogin;
	private String token;
	@JsonProperty("isactive")
	private boolean isActive;
	private List<PhoneResponse> phones;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public List<PhoneResponse> getPhones() {
		return phones;
	}
	public void setPhones(List<PhoneResponse> phones) {
		this.phones = phones;
	}
	
}
