package com.bci.usuarios.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateUserResponse {
	
	private Integer id;
	private String created;
	private String modified;
	@JsonProperty("last_login")
	private String lastLogin;
	private String token;
	@JsonProperty("isactive")
	private boolean isActive;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	
}
