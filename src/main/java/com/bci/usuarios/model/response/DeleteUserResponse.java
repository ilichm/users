package com.bci.usuarios.model.response;

public class DeleteUserResponse {
	
private String mensaje;
	
	public DeleteUserResponse(String msj) {
		this.mensaje = msj;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
