package com.bci.usuarios.model.response;

public class UpdateUserResponse {
	
	private String mensaje;
	
	public UpdateUserResponse(String msj) {
		this.mensaje = msj;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
