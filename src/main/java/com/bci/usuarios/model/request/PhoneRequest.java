package com.bci.usuarios.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PhoneRequest {
	
	private String number;
	
	@JsonProperty("citycode")
	private String cityCode;
	
	@JsonProperty("countrycode")
	private String countryCode;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
}
