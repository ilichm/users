package com.bci.usuarios.model.request;

import java.util.List;

import javax.validation.constraints.Pattern;

public class CreateUserRequest {
	
	private String name;
	@Pattern(regexp = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$", message="Formato email invalido")
	private String email;
	private String password;
	private List<PhoneRequest> phones;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<PhoneRequest> getPhones() {
		return phones;
	}
	public void setPhones(List<PhoneRequest> phones) {
		this.phones = phones;
	}

}
