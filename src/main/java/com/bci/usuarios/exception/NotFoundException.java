package com.bci.usuarios.exception;

public class NotFoundException {
	
	private String mensaje;
	
	public NotFoundException(String desc) {
		this.mensaje = desc;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
