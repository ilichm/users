package com.bci.usuarios.exception;

public class DbException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public DbException() {
		super("Error al traer registro");
	}
	
	public DbException(String msg, String descp) {
		super(msg);
		this.mensaje = "ERROR";
		this.descripcion = "descp";
	}
	
	private String mensaje;
	private String descripcion;
}
