package com.bci.usuarios.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.bci.usuarios.model.response.Message;

@ControllerAdvice
public class ControllerAdvisor {

	@ExceptionHandler(value = {DbException.class})
	public ResponseEntity<Object> registerNotFound (DbException ex) {
		NotFoundException notFound = new NotFoundException(ex.getMessage());
		return ResponseEntity.ok(notFound);
	}
	
	@ExceptionHandler(value = {MethodArgumentNotValidException.class})
	public ResponseEntity<Object> formatNotvalid () {
		Message formatNotvalid = new Message("Formato email no valido");
		return ResponseEntity.ok(formatNotvalid);
	}
	
	@ExceptionHandler(value = {PasswordFormatException.class})
	public ResponseEntity<Object> formatPasswordException (PasswordFormatException ex) {
		Message formatNotvalid = new Message(ex.getMessage());
		return ResponseEntity.ok(formatNotvalid);
	}
}
