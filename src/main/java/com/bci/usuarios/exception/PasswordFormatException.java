package com.bci.usuarios.exception;

public class PasswordFormatException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public PasswordFormatException(String msg) {
		super(msg);
	}
	
}
