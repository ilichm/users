package com.bci.usuarios.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bci.usuarios.exception.DbException;
import com.bci.usuarios.model.Phone;
import com.bci.usuarios.model.User;
import com.bci.usuarios.model.request.CreateUserRequest;
import com.bci.usuarios.model.request.PhoneRequest;
import com.bci.usuarios.model.request.UpdateUserRequest;
import com.bci.usuarios.model.response.CreateUserResponse;
import com.bci.usuarios.model.response.GetUserResponse;
import com.bci.usuarios.model.response.PhoneResponse;
import com.bci.usuarios.repository.PhoneRepository;
import com.bci.usuarios.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PhoneRepository phoneRepository;
	
	@Override
	public CreateUserResponse createUser(CreateUserRequest req) {
		
		int cantEmail = userRepository.getQuantifyEmails(req.getEmail()).intValue();
		if(cantEmail>0) {
			throw new DbException("El email ya existe", null);
		}
		CreateUserResponse userResp = new CreateUserResponse();
		
		Date date = new Date();
		
		User user = new User();
		user.setActive(true);
		user.setCreated(date.toString());
		user.setEmail(req.getEmail());
		user.setModified(date.toString());
		user.setName(req.getName());
		user.setPassword(req.getPassword());
		user.setLastLogin(date.toString());
		
		User usSaved = userRepository.saveUser(user);
		
		for(PhoneRequest p : req.getPhones()) {
			Phone phone = new Phone();
			phone.setIdUser(usSaved.getId());
			phone.setNumber(p.getNumber());
			phone.setCityCode(p.getCityCode());
			phone.setCountryCode(p.getCountryCode());
			phoneRepository.savePhone(phone);
		}
		
		userResp.setId(usSaved.getId());
		userResp.setCreated(usSaved.getCreated());
		userResp.setModified(usSaved.getModified());
		userResp.setLastLogin(usSaved.getLastLogin());
		userResp.setActive(usSaved.isActive());
		return userResp;
	}

	@Override
	public GetUserResponse getUserById(int id) {
		
		try {
		
			List<PhoneResponse> phonesResp = new ArrayList<>();
			
			User user = userRepository.getUserById(id);
			List<Phone> phones = phoneRepository.getPhonesByIdUser(id);
			
			for(Phone p : phones) {
				PhoneResponse phoneResp = new PhoneResponse();
				phoneResp.setCityCode(p.getCityCode());
				phoneResp.setCountryCode(p.getCountryCode());
				phoneResp.setNumber(p.getNumber());
				phonesResp.add(phoneResp);
			}
					
			GetUserResponse userResp = new GetUserResponse();
			
			userResp.setId(user.getId());
			userResp.setName(user.getName());
			userResp.setActive(user.isActive());
			userResp.setCreated(user.getCreated());
			userResp.setLastLogin(user.getLastLogin());
			userResp.setModified(user.getModified());
			userResp.setPhones(phonesResp);
			
			return userResp;
		} catch (NullPointerException ex) {
			throw new DbException("El registro no existe en bd", ex.getMessage());
		}
	}

	@Override
	public void updateUser(UpdateUserRequest req, Integer id) {
		
		try {
			
			int cantEmail = userRepository.getQuantifyEmails(req.getEmail()).intValue();
			if(cantEmail>0) {
				throw new DbException("El email ya existe", null);
			} 
			
			Date date = new Date();
			User user = userRepository.getUserById(id);
			user.setName(req.getName());
			user.setModified(date.toString());
			user.setEmail(req.getEmail());
			user.setPassword(req.getPassword());
			userRepository.updateUser(user);
		} catch (NullPointerException ex) {
			throw new DbException("El registro no existe en bd", ex.getMessage());
		}
	}

	@Override
	public void deleteUser(Integer id) {
		try {
			User user = userRepository.getUserById(id);
			userRepository.deleteUserById(user);
		} catch (IllegalArgumentException | NullPointerException ex) {
			throw new DbException("El registro no existe en bd", ex.getMessage());
		}
	}
	
}
