package com.bci.usuarios.service;

import com.bci.usuarios.model.request.CreateUserRequest;
import com.bci.usuarios.model.request.UpdateUserRequest;
import com.bci.usuarios.model.response.CreateUserResponse;
import com.bci.usuarios.model.response.GetUserResponse;

public interface UserService {
	
	GetUserResponse getUserById(int id);
	CreateUserResponse createUser(CreateUserRequest req);
	void updateUser(UpdateUserRequest req, Integer id);
	void deleteUser(Integer id);
}
