package com.bci.usuarios.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bci.usuarios.model.Phone;

@Component
public class PhoneRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void savePhone (Phone phone) {
		em.persist(phone);
	}
	
	@Transactional(readOnly = true)
	public List<Phone> getPhonesByIdUser (Integer id) {
		@SuppressWarnings("unchecked")
		List<Phone> phones = em.createNamedQuery("Phone.findPhonesByIdUser")
				.setParameter("idUser", id)
				.getResultList();
		return phones;
	}

}
