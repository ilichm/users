package com.bci.usuarios.repository;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bci.usuarios.model.User;

@Component
public class UserRepository {

	@PersistenceContext
	private EntityManager em;

	@Transactional(readOnly = true)
	public User getUserById(int id) {
		User user = (User)em.find(User.class, id);
		return user;
	}
	
	@Transactional
	public User saveUser(User user) {
		User us = null;
	        try {
	            us = em.merge(user);

	            if (us != null) {
	                em.lock(us, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
	                em.flush();
	            }
	        } catch (OptimisticLockException ole) {
	            return null;
	        }
			return us;
	}
	
	@Transactional
	public void updateUser(User user) {
        em.merge(user);
	}
	
	@Transactional
	public void deleteUserById(User user) {
        em.remove(user);
	}
	
	@Transactional(readOnly = true)
	public Long getQuantifyEmails (String email) {
		Long cant = (Long) em.createNamedQuery("User.selectEmail")
				.setParameter("email", email).getSingleResult();
		return cant;
	}
}
